export default {
    '/': 'Home',
    // '/hello': 'HelloWorld',
    '/notes': 'Notebook',
    '/books': 'BookShelf',
    '/notes/retrieve': 'Book',
  }