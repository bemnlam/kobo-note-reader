#!/bin/zsh
NEXT_VER=$(jq --raw-output ".version" kobo-note-reader/package.json)
git tag v${NEXT_VER}
git tag