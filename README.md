# Kobo Note Reader

![favicon](https://kobo.lofibean.cc/favicon-32x32.png)

![pipeline status](https://gitlab.com/bemnlam/kobo-note-reader/badges/master/pipeline.svg)

A web application to read highlighted content and notes in Kobo reader.

Demo: https://kobo.lofibean.cc

## How to use

### Upload by url
Paste the url of the sqlite file and submit.
### Upload local file
1. Go to the homepage
2. Upload the `KoboReader.sqlite` of your Kobo reader. The file is under `(Kobo reader root directory)/.kobo`

> macOS user: click the upload file input -> **Cmd** + **Shift** + **G** -> type `/Volumes/KOBOeReader/.kobo`


## Up and run

```bash
cd kobo-note-reader
yarn install
yarn serve
```

The app will up and run at http://localhost:8080.

## What is it about?

In each Kobo reader, there is a `.kobo/KoboReader.sqlite` file to store all the highlighted text and notes.

```
└── .kobo
    ├── BookReader.sqlite
    ├── KoboReader.sqlite
    └── (other files...)
```

This application cna read a given `KoboReader.sqlite`, list out all the books the reader has the related notes / highlights.

## Tools
- [ Vue Cli ](https://cli.vuejs.org/)
- [ Quasar Framework ](https://github.com/quasarframework/quasar/)
- [ sql-js/sql.js ](https://github.com/sql-js/sql.js/)
- [ favicon.io ](https://favicon.io/favicon-converter/)
- [ Figma ]([Figma](https://www.figma.com/file/aCRp1eprZKNdRpWdvQo38d/KNR-App-Icon-Toolkit?node-id=9%3A1170))