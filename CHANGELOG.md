# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

(no)

## 🏷 [2.3.3] - (2022-02-12)

- Fix build errors
  - Replace `node-sass` with `sass`
  - Pin Node.js version in build pipeline

## 🏷 [2.3.2] - (2022-02-12)

- Change site path to https://kobo.lofibean.cc.

## 🎗 [2.3.1] - (2021-02-28)
### Changed
- Update app icon.

## 🏷 [2.3.0] - (2021-02-28)
### Added
- Upload file by url.

### Changed
- Become iOS Web app by "Add to homescreen".
- Adjust mobile UI.

## 🏷 [2.2.1] - (2021-02-27)
- Fix mobile UI.

## 🏷 [2.2.0] - (2021-02-27)
### Added
- Demo mode.
### Changed
- Minor update on UI.

## 🏷 [2.1.0] - (2021-02-27)
### Changed
- Sync UI and functions across pages.

## 🏷 [2.0.1] - (2021-02-13)
### Added
- file upload instruction.
### Changed
- Fix note sorting.

## 🏷 [2.0.0] - (2021-02-13)
### Added
- Notebook page showing all notes with copying/sharing.

## 🏷 [1.1.1] - (2021-02-13)
- Change site path to https://kobo.eternalrecurrence.space.

## 🏷 [1.1.0] - (2020-09-06)
### Changed
- Branding and app icon.

## 🏷 [1.0.0] - (2020-08-31)
### Added
- Support upload and read sqllite file.

## 🏷 [0.1.0] - (2020-08-30)
- Initial release.
